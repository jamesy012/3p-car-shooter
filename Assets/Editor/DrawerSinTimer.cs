﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(SinTimer)),CanEditMultipleObjects]
public class DrawerSinTimer : PropertyDrawer {

	private bool m_Show = false;
	private int m_Values = 0;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		m_Values = 0;
		position = new Rect(position.x, position.y, position.width, 16);

		EditorGUI.BeginProperty(position, label, property);
		m_Show = EditorGUI.Foldout(position, m_Show, label);

		// Draw label
		//position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
		EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

		// Don't make child fields be indented
		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = indent + 1;


		if (m_Show) {
			m_Values += 5;

			SerializedProperty amplitude = property.FindPropertyRelative("m_Amplitude");
			SerializedProperty frequency = property.FindPropertyRelative("m_Frequency");
			SerializedProperty phase = property.FindPropertyRelative("m_Phase");
			SerializedProperty offset = property.FindPropertyRelative("m_Offset");

			AnimationCurve ac = new AnimationCurve();
			int points = 45;

			for (int i = 0; i < points; i++) {
				float time = i / (float)points;
				float value = amplitude.floatValue * Mathf.Sin(frequency.floatValue * i + phase.floatValue) + offset.floatValue;
				Keyframe kf = new Keyframe(time, value);
				ac.AddKey(kf);
				AnimationUtility.SetKeyLeftTangentMode(ac, i, AnimationUtility.TangentMode.Auto);
				AnimationUtility.SetKeyRightTangentMode(ac, i, AnimationUtility.TangentMode.Auto);
				if (i != 0) {
					ac.SmoothTangents(i-1, 1.0f);
				}
			}

			position = new Rect(position.x, position.y + position.height, position.width, position.height);
			EditorGUI.PropertyField(position, amplitude);
			position = new Rect(position.x, position.y + position.height, position.width, position.height);
			EditorGUI.PropertyField(position, frequency);
			position = new Rect(position.x, position.y + position.height, position.width, position.height);
			EditorGUI.PropertyField(position, phase);
			position = new Rect(position.x, position.y + position.height, position.width, position.height);
			EditorGUI.PropertyField(position, offset);

			//EditorGUI.LabelField(position, label);

			//base.OnGUI(position, property, label);


			Rect CurvePos = new Rect(position.x, position.y + position.height, position.width, position.height);
			EditorGUI.CurveField(CurvePos, ac);
		}

		// Set indent back to what it was
		EditorGUI.indentLevel = indent;

		EditorGUI.EndProperty();
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
		return base.GetPropertyHeight(property, label) + (16 * m_Values);
	}

}
