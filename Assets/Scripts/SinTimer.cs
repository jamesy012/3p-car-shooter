﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SinTimer {

	public float m_Amplitude = 1.0f;
	public float m_Frequency = 1.0f;
	public float m_Phase = 0.0f;
	public float m_Offset = 0.0f;


	public float getValue() {
		return m_Amplitude * Mathf.Sin(m_Frequency * Time.frameCount + m_Phase) + m_Offset;
	}

	public float getValue(float a_Time) {
		return m_Amplitude * Mathf.Sin(m_Frequency * a_Time + m_Phase) + m_Offset;
	}

}
