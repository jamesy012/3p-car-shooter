﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCameraPosition : MonoBehaviour {

    public Vector3 m_Offset;
    public Vector3 m_Rotation;
    public float m_Distance;
    private Transform m_CarTransform;

    // Use this for initialization
    void Start() {
        m_CarTransform = transform.parent;
    }


    // Update is called once per frame
    void Update() {
        transform.position = getPosition();
        transform.LookAt(getOffset());
    }

    public void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        m_CarTransform = transform.parent;
        Gizmos.DrawLine(getOffset(), getPosition());
        Gizmos.DrawSphere(getPosition(), 0.2f);

    }

    public Vector3 getPosition() {
        Vector3 positionOffset = getOffset();
        Vector3 rotOffset = m_CarTransform.rotation * (Quaternion.Euler(m_Rotation) * -Vector3.forward) * m_Distance;
        return positionOffset + rotOffset;
    }

    public Vector3 getOffset() {
        return m_CarTransform.position + (m_CarTransform.rotation * m_Offset);
    }
}
