﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : ICamera {

    public Transform m_FollowObject;
    public Transform m_CarObject;

    private Vector3 m_DisiredLocation;

    public float m_MoveSpeed = 5;
    public float m_RotateSpeed = 5;

    private Vector3 m_Offset;
    private Vector3 m_Dir;
    private float m_Distance;
    private RaycastHit m_Hit;

    public override void cameraUpdate() {
        base.cameraUpdate();

        if (m_FollowObject == null) {
            return;
        }

        float speed = m_MoveSpeed * Time.deltaTime;
        float distance = Vector3.Distance(m_ICCamera.transform.position, m_FollowObject.position);
        speed *= distance * 0.75f;
        //print("Speed: " + speed + " Distance: " + distance);
        Vector3 newPos = Vector3.MoveTowards(m_ICCamera.transform.position, m_FollowObject.position, speed);


        //ground test/ downwards raycast
        // {
        //     m_Offset = Vector3.up;
        //     m_Dir = Vector3.down;
        //     m_Distance = 3;
        //     if (didHit()) {
        //         float dist = m_Distance - m_Hit.distance;
        //         //print("hit " + dist);
        //         newPos += Vector3.up * dist;
        //         // transform.rotation *= rot;
        //     }
        // }


        m_ICCamera.transform.position = newPos;
        //m_ICCamera.transform.LookAt(m_CarObject);
        //m_ICCamera.transform.position = m_FollowObject.position;
        // m_ICCamera.transform.rotation = m_FollowObject.rotation;
        float diff = Quaternion.Angle(m_ICCamera.transform.rotation, m_FollowObject.rotation);
        print(diff);
        if (diff <= 3) {
            float ratio = (3 - diff) / 3;
            print("ratio " + ratio);
            m_ICCamera.transform.rotation = Quaternion.Slerp(m_ICCamera.transform.rotation, m_FollowObject.rotation, Time.deltaTime * m_RotateSpeed * (2 * ratio));
        } else {
            m_ICCamera.transform.rotation = Quaternion.Slerp(m_ICCamera.transform.rotation, m_FollowObject.rotation, Time.deltaTime * m_RotateSpeed);
        }
    }

    private bool didHit() {
        Debug.DrawRay(m_ICCamera.transform.position, m_Dir * m_Distance, Color.blue);
        return Physics.Raycast(m_ICCamera.transform.position + m_Offset, m_Dir, out m_Hit, m_Distance);
    }
}
