﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsCamera : ICamera {

	public AnimationCurve m_MovementCurve;
	public AnimationCurve m_RotationCurve;

	public Timer m_Timer;

	private Quaternion m_StartRot;
	private Vector3 m_StartPos;
	public Transform m_MoveTarget;

	public override void cameraStart() {
		base.cameraStart();
		m_Timer.startTimer();
		m_StartPos = m_ICMover.transform.position;
		m_StartRot = m_ICMover.transform.rotation;
		m_IsDone = false;
	}


	public override void cameraUpdate() {
		base.cameraUpdate();
		if (m_Timer.isDone()) {
			//have a bool as well to stop the whole update?!
			m_IsDone = true;
			return;
		}

		float percentage = m_Timer.getPercentage();

		float movementPercentage = m_MovementCurve.Evaluate(percentage);
		float rotationPercentage = m_RotationCurve.Evaluate(percentage);

		Vector3 pos = Vector3.Lerp(m_StartPos, m_MoveTarget.position, movementPercentage);
		Quaternion rot = Quaternion.Slerp(m_StartRot, m_MoveTarget.rotation, rotationPercentage);

		m_ICMover.position = pos;
		m_ICMover.rotation = rot;

	}

}
