﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrustumDrawer : MonoBehaviour {

	public void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.DrawFrustum(transform.position, Camera.main.fieldOfView, Camera.main.farClipPlane, Camera.main.nearClipPlane, Camera.main.aspect);

	}
}
