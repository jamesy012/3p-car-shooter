﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ICamera : MonoBehaviour {

	[HideInInspector]
	public Camera m_ICCamera = null;
	[HideInInspector]
	public Transform m_ICMover = null;

	
	public bool m_IsDone = false;

	public virtual void cameraStart() {
		m_IsDone = true;
	}

	public virtual void cameraUpdate() {

	}



}
