﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsCameraLookAt : ICamera {

	public AnimationCurve m_MovementCurve;

	public Timer m_Timer;
	
	private Vector3 m_StartPos;

	public Transform m_MoveTarget;
	public Transform m_LookTarget;

	public override void cameraStart() {
		base.cameraStart();
		m_Timer.startTimer();
		m_StartPos = m_ICMover.transform.position;
		m_IsDone = false;
	}


	public override void cameraUpdate() {
		base.cameraUpdate();
		if (m_Timer.isDone()) {
			//have a bool as well to stop the whole update?!
			m_IsDone = true;
			return;
		}

		float percentage = m_Timer.getPercentage();

		float movementPercentage = m_MovementCurve.Evaluate(percentage);

		Vector3 pos = Vector3.Lerp(m_StartPos, m_MoveTarget.position, movementPercentage);

		m_ICMover.position = pos;
		m_ICMover.LookAt(m_LookTarget);

	}
}
