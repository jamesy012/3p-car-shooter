﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandHeldCamera : ICamera {

	public float m_MaxDistanceFromStart = 2.0f;
	public float m_MovementScale = 0.5f;
	public Transform m_LookAtPos;

	private Vector3 m_Velocity = Vector3.zero;

	public Transform m_StartPos;

	[System.Serializable]
	public struct Movement {
		public SinTimer m_MovementWave;
		[HideInInspector]
		public float m_SinTime;
		public float m_MovementTimeScale;
	}

	public Movement m_XMovementTimer;
	public Movement m_YMovementTimer;

	public float m_CameraSlerpScale = 0.5f;

	public override void cameraStart() {

		base.cameraStart();
		m_IsDone = true;
		if (m_LookAtPos != null) {
			transform.LookAt(m_LookAtPos);
		}

	}

	public override void cameraUpdate() {
		base.cameraUpdate();

		//m_SinTime += 5.0f * Mathf.Sin(25.0f * Time.frameCount + Random.Range(-1.0f,1.0f));
		m_XMovementTimer.m_SinTime += m_XMovementTimer.m_MovementWave.getValue(Time.frameCount + Random.Range(-1.0f, 1.0f));
		m_YMovementTimer.m_SinTime += m_YMovementTimer.m_MovementWave.getValue(Time.frameCount + Random.Range(-1.0f, 1.0f));
		float sinScale = m_MovementScale * Mathf.Sin(m_XMovementTimer.m_MovementTimeScale * m_XMovementTimer.m_SinTime);
		m_Velocity.x += sinScale;
		sinScale = m_MovementScale * Mathf.Sin(m_YMovementTimer.m_MovementTimeScale * m_YMovementTimer.m_SinTime);
		m_Velocity.y += sinScale;
		m_Velocity *= 0.9f;
		Vector3 nextPosition = getCurrentPosition();//get position

		//add position offset
		//nextPosition += nextOffset * Time.deltaTime;
		nextPosition += m_Velocity * Time.deltaTime;


		//check to see if it's a place we can go
		{
			float distanceFromStart = Vector3.Distance(nextPosition, getStartPos());
			if (distanceFromStart > m_MaxDistanceFromStart) {
				nextPosition = getStartPos() + (nextPosition - getStartPos()).normalized * m_MaxDistanceFromStart;
			}
		}


		//apply new position and look at m_LookAtPos if it exits
		//m_Velocity = nextOffset;
		//if (m_LookAtPos != null) {
		//	transform.LookAt(m_LookAtPos);
		//}
		applyCurrentPosition(nextPosition);

		m_ICMover.rotation = Quaternion.Slerp(m_ICMover.rotation, Quaternion.LookRotation(m_LookAtPos.position - m_ICMover.position), Time.deltaTime * m_CameraSlerpScale);
	}

	public void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;

		Gizmos.DrawWireSphere(getStartPos(), m_MaxDistanceFromStart);

	}

	private Vector3 getStartPos() {

		return m_StartPos.position;
	}

	private Vector3 getCurrentPosition() {
		return m_ICMover.position;
		//return transform.localPosition;
	}

	private void applyCurrentPosition(Vector3 a_Pos) {
		m_ICMover.position = a_Pos;
		//transform.localPosition = a_Pos ;
	}
}
