﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public enum CAMERA_AND_MENU_STATES {
		StartingCam,
		TravelToCinimaticStartCam,
		TravelToCarCam,
		PlayCam,
		ReturnToStartCam,
		STATES_END
	}

	[System.Serializable]
	public struct CameraScriptState {
		[HideInInspector]
		public string m_Name;
		//[HideInInspector]
		//public STATES m_State;
		public ICamera m_Script;
	}

	public CameraScriptState[] m_CameraScripts = new CameraScriptState[(int)CAMERA_AND_MENU_STATES.STATES_END];

	private ICamera m_CurrentScript;
	[SerializeField]
	private CAMERA_AND_MENU_STATES m_CurrentState;
	/// <summary>
	/// this will be the real value for m_CurrentState;
	/// this way, m_CurrentState appears in inspector and if it is edited, it will return to the correct value
	/// </summary>
	private CAMERA_AND_MENU_STATES m_BackupCurrentState;

	public Transform m_CameraMover;

	// Use this for initialization
	void Awake() {
		//debug starting of states
		m_BackupCurrentState = m_CurrentState;

		Camera camera = Camera.main;
		if(m_CameraMover == null) {
			Debug.LogError("Add Camera Mover into CameraController!");
		}
		for (int i = 0; i < m_CameraScripts.Length; i++) {
			ICamera ic = m_CameraScripts[i].m_Script;
			if(ic == null) {
				continue;
			}
			ic.m_ICCamera = camera;
				ic.m_ICMover = m_CameraMover;
		}
		
	}

	public void Start() {
		switchState(getCurrentState());
		//if (m_CurrentScript != null) {
		//	m_CurrentScript.cameraStart();
		//}
	}


	// Update is called once per frame
	void FixedUpdate() {
		m_CurrentState = m_BackupCurrentState;
		if (m_CurrentScript != null) {
			m_CurrentScript.cameraUpdate();
		}

		if (Input.GetKeyDown(KeyCode.Space)) {
			switchState(m_CurrentState ^ CAMERA_AND_MENU_STATES.TravelToCinimaticStartCam);
		}
	}

	public void OnValidate() {
		if (m_CameraScripts.Length != (int)CAMERA_AND_MENU_STATES.STATES_END) {
			Debug.LogError("Length of m_CameraScripts array is not of size " + (int)CAMERA_AND_MENU_STATES.STATES_END + " (Possible loss of data)");
			CameraScriptState[] newCss = new CameraScriptState[(int)CAMERA_AND_MENU_STATES.STATES_END];
			for (int i = 0; i < m_CameraScripts.Length; i++) {
				if(i >= (int)CAMERA_AND_MENU_STATES.STATES_END) {
					break;
				}
				newCss[i] = m_CameraScripts[i];
			}
			m_CameraScripts = newCss;
			}
		for (int i = 0; i < m_CameraScripts.Length; i++) {
			CAMERA_AND_MENU_STATES state = (CAMERA_AND_MENU_STATES)i;
			m_CameraScripts[i].m_Name = state.ToString();
			//m_CameraScripts[i].m_State = state;
		}
	}

	public void switchState(CAMERA_AND_MENU_STATES a_State) {
		if (a_State != CAMERA_AND_MENU_STATES.STATES_END) {
			int index = (int)a_State;
			if (m_CameraScripts[index].m_Script != null) {
				m_BackupCurrentState = a_State;
				m_CurrentScript = m_CameraScripts[index].m_Script;
				m_CurrentScript.cameraStart();
			}
		}
	}

	public void switchState(int a_State) {
		if(a_State >= (int)CAMERA_AND_MENU_STATES.STATES_END) {
			return;
		}
		CAMERA_AND_MENU_STATES state = (CAMERA_AND_MENU_STATES)a_State;
		switchState(state);
	}

	public CAMERA_AND_MENU_STATES getCurrentState() {
		return m_BackupCurrentState;
	}

	public ICamera getCurrentScript() {
		return m_CurrentScript;
	}

}
