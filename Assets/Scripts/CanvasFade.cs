﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasFade : MonoBehaviour {

	private CanvasRenderer[] m_CanvasElements;

	public AnimationCurve m_AlphaCurve;

	public Timer m_Timer;

	public bool m_FadingIn = true;
	public bool m_FadeOnStart = true;

	// Use this for initialization
	void Start () {
		m_CanvasElements = GetComponentsInChildren<CanvasRenderer>();
		if (m_FadeOnStart) {
			if (m_FadingIn) {
				startFadeIn();
			} else {
				startFadeOut();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!m_Timer.isDone()) {
			float timeOnCurve;
			if (m_FadingIn) {
				timeOnCurve = m_Timer.getPercentage();
			} else {
				timeOnCurve = 1 - m_Timer.getPercentage();
			}
			setAlphaOfAllElements(m_AlphaCurve.Evaluate(timeOnCurve));
		}
	}

	public void startFadeIn() {
		m_FadingIn = true;
		m_Timer.startTimer();
		setAlphaOfAllElements(0.0f);
	}

	public void startFadeOut() {
		m_FadingIn = false;
		m_Timer.startTimer();
		setAlphaOfAllElements(1.0f);
	}

	private void setAlphaOfAllElements(float a_Alpha) {
		//!!! if window is scaled all elements return to their alpha to 1 when using CanvasRenderers

		for(int i = 0; i < m_CanvasElements.Length; i++) {
			m_CanvasElements[i].SetAlpha(a_Alpha);
		}
	}
}
