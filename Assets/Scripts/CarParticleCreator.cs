﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
[RequireComponent(typeof(TerrainTextureAtPoint))]
public class CarParticleCreator : MonoBehaviour {

    private Terrain terrain;
    private TerrainData terrainData;
    private Vector3 terrainPos;

    private ParticleSystem m_Particles;
    private Car m_Car;
    private TerrainTextureAtPoint m_Ttap;

    private ParticleSystem.MainModule m_ParticleMainModule;
    private ParticleSystem.EmissionModule m_ParticleEmissionModule;

    [System.Serializable]
    public struct TextureTypeColors {
        [HideInInspector]
        public string m_Name;
        public Color m_Color;
    }

    public TextureTypeColors[] m_TextureColors = new TextureTypeColors[(int)TerrainTextureTypes.TYPES_END];

    public AnimationCurve m_ParticleEmissionCurve;

    // Use this for initialization
    void Start() {
        terrain = Terrain.activeTerrain;
        terrainData = terrain.terrainData;
        terrainPos = terrain.transform.position;

        m_Particles = GetComponent<ParticleSystem>();
        m_Car = GetComponentInParent<Car>();
        m_Ttap = GetComponent<TerrainTextureAtPoint>();

        m_ParticleMainModule = m_Particles.main;
        m_ParticleEmissionModule = m_Particles.emission;


        m_ParticleEmissionModule.rateOverTime = 0.0f;
    }

    // Update is called once per frame
    void Update() {
        if (!m_Car.isOnGround) {
            m_ParticleEmissionModule.rateOverTime = 0;
            return;
        }

        TerrainTextureTypes index = m_Ttap.getTextureAtPoint();
        //print(index);
        m_ParticleEmissionModule.rateOverTime = m_ParticleEmissionCurve.Evaluate(m_Car.m_CurrSpeed.magnitude/m_Car.m_MaxSpeed) * 40;
        m_ParticleMainModule.startColor = m_TextureColors[(int)index].m_Color;
        //switch (index) {
        //    case TerrainTextureTypes.Dirt:
        //        m_ParticleMainModule.startColor = Color.white;
        //        break;
        //    case TerrainTextureTypes.Road:
        //        m_ParticleMainModule.startColor = Color.black;
        //        break;
        //}
        //m_Particles.ma = mm;
    }

    void OnValidate() {
        if (m_TextureColors.Length != (int)TerrainTextureTypes.TYPES_END) {
            Debug.LogError("Length of m_TextureColors array is not of size " + (int)TerrainTextureTypes.TYPES_END + " (Possible loss of data)");
            TextureTypeColors[] newTtc = new TextureTypeColors[(int)TerrainTextureTypes.TYPES_END];
            for (int i = 0; i < m_TextureColors.Length; i++) {
                if (i >= (int)TerrainTextureTypes.TYPES_END) {
                    break;
                }
                newTtc[i] = m_TextureColors[i];
            }
            m_TextureColors = newTtc;
        }
        for (int i = 0; i < m_TextureColors.Length; i++) {
            TerrainTextureTypes state = (TerrainTextureTypes)i;
            m_TextureColors[i].m_Name = state.ToString();
            //m_CameraScripts[i].m_State = state;
        }
    }

}
