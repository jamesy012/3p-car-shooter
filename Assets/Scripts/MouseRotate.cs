﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RestoreStartPositionAfterTime))]
public class MouseRotate : MonoBehaviour {

    public float m_Speed;

    private RestoreStartPositionAfterTime m_Rspat;

    public Vector3 m_StartingRot;

    void Start() {

        m_Rspat = GetComponent<RestoreStartPositionAfterTime>();
    }
	
	// Update is called once per frame
	void Update () {
        m_StartingRot = transform.rotation.eulerAngles;
        if (!MouseLock.m_Singleton.isLocked) {
            return;
        }
        Vector3 mouseMovement = new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);

        float minAmount = 0.05f;
        if(Mathf.Abs(mouseMovement.x) > minAmount || Mathf.Abs(mouseMovement.y) > minAmount) {
            m_Rspat.updateTime();
        }else {
            return;                    
        }

        transform.Rotate(mouseMovement * m_Speed * Time.deltaTime);

        Vector3 rotation = transform.rotation.eulerAngles;
        rotation.z = 0;
        float rotX = 360 - rotation.x;
        //print(rotX);
        if(rotX > 20 && rotX < 260) {
            rotation.x = 0;
        }
        transform.rotation = Quaternion.Euler(rotation);
	}
}
