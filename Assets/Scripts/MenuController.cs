﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {

	private PlayerCar m_PlayerCar;
	private Vector3 m_PlayerCarStartPos;
	private Quaternion m_PlayerCarStartRot;


	public UnityEngine.UI.Text m_InstructionsText;

	private bool m_HasPressedForward = false;

	public AiCar m_ChaseCar;

	private float m_StartingFriction;

	/// <summary>
	/// for current state (maybe should move into this script?)
	/// </summary>
	private CameraController m_CameraController;

	// Use this for initialization
	void Awake() {
		m_CameraController = FindObjectOfType<CameraController>();
		m_PlayerCar = FindObjectOfType<PlayerCar>();

		if (m_PlayerCar == null || m_CameraController == null) {
			Debug.LogError("Script cant be found");
			return;
		}

		m_PlayerCarStartPos = m_PlayerCar.transform.position;
		m_PlayerCarStartRot = m_PlayerCar.transform.rotation;

		//m_PlayerCar.m_PlayerControl = false;

		m_ChaseCar.enabled = false;
		m_StartingFriction = m_PlayerCar.m_Friction;

		m_InstructionsText.enabled = false;

	}

	public void Update() {
		if (m_CameraController.getCurrentState() == CameraController.CAMERA_AND_MENU_STATES.TravelToCinimaticStartCam) {
			if (m_CameraController.getCurrentScript().m_IsDone) {
				if (m_HasPressedForward) {

					m_PlayerCar.vertical = 1.0f;
					m_ChaseCar.enabled = true;

				} else {
					m_InstructionsText.enabled = true;
					m_InstructionsText.GetComponent<CanvasRenderer>().SetAlpha(1.0f);
					if (Input.GetAxis("Vertical") > 0.5f) {
						m_HasPressedForward = true;
					}
				}
			}
		}
		if (m_CameraController.getCurrentState() == CameraController.CAMERA_AND_MENU_STATES.TravelToCarCam) {
			if (m_CameraController.getCurrentScript().m_IsDone) {
				m_CameraController.switchState(CameraController.CAMERA_AND_MENU_STATES.PlayCam);
				
			} else {

			}
		}
	}

	public void OnTriggerEnter(Collider other) {
		if (m_CameraController.getCurrentState() == CameraController.CAMERA_AND_MENU_STATES.TravelToCinimaticStartCam) {
			if (other.gameObject.GetComponentInParent<PlayerCar>() != null) {
				m_CameraController.switchState(CameraController.CAMERA_AND_MENU_STATES.TravelToCarCam);
				m_InstructionsText.enabled = false;

                MouseLock.m_Singleton.lockMouse();
                MouseLock.m_Singleton.m_AllowInputControls = true;
                //m_PlayerCar.m_Friction = 0;

                Rigidbody rb = m_PlayerCar.GetComponent<Rigidbody>();
				rb.angularVelocity = Vector3.zero;
			}
		}
	}

	public void OnTriggerExit(Collider other) {
		if (m_CameraController.getCurrentState() == CameraController.CAMERA_AND_MENU_STATES.TravelToCarCam) {
			if (other.gameObject.GetComponentInParent<PlayerCar>() != null) {
				m_PlayerCar.m_PlayerControl = true;
				m_PlayerCar.m_Friction = m_StartingFriction;//todo slowly turn friction back on?
			}
		}
	}
}
