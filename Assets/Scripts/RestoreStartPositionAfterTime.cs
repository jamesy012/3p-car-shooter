﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestoreStartPositionAfterTime : MonoBehaviour {

    public float m_TimeTillReset;
    private Timer m_Time = new Timer();

    private Vector3 m_StartingPosition;
    private Quaternion m_StartingRotation;

    public float m_LerpSpeed = 5;

	// Use this for initialization
	void Start () {
        m_StartingPosition = transform.localPosition;
        m_StartingRotation = transform.localRotation;

        m_Time.m_TimeTaken = m_TimeTillReset;
        m_Time.startTimer();
    }
	
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
        m_Time.m_TimeTaken = m_TimeTillReset;
#endif

        if (m_Time.isDone()) {
            transform.localPosition = Vector3.Lerp(transform.localPosition, m_StartingPosition, Time.deltaTime * m_LerpSpeed);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, m_StartingRotation, Time.deltaTime * m_LerpSpeed);
        }
    }

    public void updateTime() {
        m_Time.startTimer();
    }

    public Vector3 getStartingPosition() {
        return m_StartingPosition;
    }

    public Quaternion getStartingRotation() {
        return m_StartingRotation;
    }
}
