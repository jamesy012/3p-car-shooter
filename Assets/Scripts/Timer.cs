﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[System.Serializable]
public class Timer {

	[FormerlySerializedAs("m_TimeTaken")]
	public float m_TimeTaken = 1;
	private float m_StartTime;
	private bool m_IsRunning = false;


	public void startTimer() {
		m_StartTime = Time.time;
		m_IsRunning = true;
	}

	public bool isDone() {
		checkIfFinished();
		return !m_IsRunning;
	}

	public float getPercentage() {
		return getTimeLeft() / m_TimeTaken;
	}

	public float getTimeLeft() {
		if (!m_IsRunning) {
			return 0;
		}
		float timeLeft = (Time.time - m_StartTime);
		return timeLeft;
	}

	private void checkIfFinished() {
		if (m_IsRunning) {
			if (getPercentage() >= 1) {
				m_IsRunning = false;
			}
		}
	}


}
