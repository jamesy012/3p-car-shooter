﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TerrainTextureTypes {
    Dirt = 0,
    Road = 1,
    TYPES_END,
}

public class TerrainTextureAtPoint : MonoBehaviour {

    private Terrain m_Terrain;
    private TerrainData m_TerrainData;
    private Vector3 m_TerrainPos;

    private int m_LastTimeCalulcated = 0;
    private TerrainTextureTypes m_LastType;

    public Transform m_Point;

    // Use this for initialization
    void Awake () {
        m_Terrain = Terrain.activeTerrain;
        m_TerrainData = m_Terrain.terrainData;
        m_TerrainPos = m_Terrain.transform.position;
    }

    private void updateTypeUnder() {
        //if the frame count is not the same, then we will update the terrainTextureType
        //could change to difference in position
        if(m_LastTimeCalulcated != Time.frameCount) {
            m_LastTimeCalulcated = Time.frameCount;
            int mainTexture = GetMainTexture(m_Point.position);
            m_LastType = (TerrainTextureTypes)mainTexture;
        }
    }

    public TerrainTextureTypes getTextureAtPoint() {
        updateTypeUnder();
        return m_LastType;
    }

    public TerrainTextureTypes getTextureAtPoint(Vector3 a_Position) {
        int mainTexture = GetMainTexture(a_Position);
        return (TerrainTextureTypes)mainTexture;
    }

    //http://answers.unity3d.com/questions/456973/getting-the-texture-of-a-certain-point-on-terrain.html
    private float[] GetTextureMix(Vector3 WorldPos) {
        // returns an array containing the relative mix of textures
        // on the main terrain at this world position.

        // The number of values in the array will equal the number
        // of textures added to the terrain.

        // calculate which splat map cell the worldPos falls within (ignoring y)
        int mapX = (int)(((WorldPos.x - m_TerrainPos.x) / m_TerrainData.size.x) * m_TerrainData.alphamapWidth);
        int mapZ = (int)(((WorldPos.z - m_TerrainPos.z) / m_TerrainData.size.z) * m_TerrainData.alphamapHeight);

        // get the splat data for this cell as a 1x1xN 3d array (where N = number of textures)
        float[,,] splatmapData = m_TerrainData.GetAlphamaps(mapX, mapZ, 1, 1);

        // extract the 3D array data to a 1D array:
        float[] cellMix = new float[splatmapData.GetUpperBound(2) + 1];

        for (int n = 0; n < cellMix.Length; n++) {
            cellMix[n] = splatmapData[0, 0, n];
        }
        return cellMix;
    }

    private int GetMainTexture(Vector3 WorldPos) {
        // returns the zero-based index of the most dominant texture
        // on the main terrain at this world position.
        float[] mix = GetTextureMix(WorldPos);

        float maxMix = 0;
        int maxIndex = 0;

        // loop through each mix value and find the maximum
        for (int n = 0; n < mix.Length; n++) {
            if (mix[n] > maxMix) {
                maxIndex = n;
                maxMix = mix[n];
            }
        }
        return maxIndex;
    }

    void OnValidate() {
        if(m_Point == null) {
            m_Point = transform;
        }
    }

}
