﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLock : MonoBehaviour {

    public static MouseLock m_Singleton;

    public bool isLocked { get { return m_IsLocked; } }

    private bool m_IsLocked = false;

    [SerializeField]
    private bool m_LockOnStart = false;
    [SerializeField]
    public bool m_AllowInputControls;

    void Start() {
        m_Singleton = this;
        if (m_LockOnStart) {
            lockMouse();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (m_AllowInputControls) {
            if (Input.GetMouseButtonDown(0)) {
                lockMouse();
            }
            if (Input.GetKeyDown(KeyCode.Escape)) {
                unlockMouse();
            }
        }
	}

    public void lockMouse() {
        Cursor.lockState = CursorLockMode.Locked;
        m_IsLocked = true;
    }

    public void unlockMouse() {
        Cursor.lockState = CursorLockMode.None;
        m_IsLocked = false;
    }

    public void allowInputControls(bool a_Allow) {
        m_AllowInputControls = a_Allow;
    }
}
