﻿using UnityEngine;
using System.Collections;

public class UpsideDownFlip : MonoBehaviour {

	float m_Timer = 0;
	public float m_TimeTillFlip = 2.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float dotUp = Vector3.Dot(transform.up, Vector3.down);
		if (dotUp > -0.2f){
			m_Timer += Time.deltaTime;
			if(m_Timer >= m_TimeTillFlip) {
				transform.rotation = Quaternion.identity;
				transform.position += new Vector3(0, 3, 0);
			}
		}
		else {
			m_Timer = 0;
		}
	}
}
