﻿using UnityEngine;
using System.Collections;

public class PlayerCar : Car {

	public bool m_PlayerControl = true;

	// Use this for initialization
	protected override void Start() {
		base.Start();
	}

	// Update is called once per frame
	public override void moveCar() {
		if (m_PlayerControl) {
			vertical = Input.GetAxis("Vertical");
			horizontal = Input.GetAxis("Horizontal");
		} else {
			//vertical = horizontal = 0;
		}

		base.moveCar();
	}

}
