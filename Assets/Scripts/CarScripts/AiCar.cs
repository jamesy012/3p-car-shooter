﻿using UnityEngine;
using System.Collections;

public class AiCar : Car {

	public Transform m_Target;

	// Use this for initialization
	protected override void Start () {
		base.Start();
		//horizontal = -1f;
		//vertical = 0.5f;
	}

	// Update is called once per frame
	public override void moveCar() {
		Vector3 targetPos = m_Target.position - transform.position;
		float dot = AngleDir(transform.forward, targetPos, transform.up);
		//Debug.DrawRay(transform.position, ( transform.right * dot) * 100, Color.green);


		horizontal = dot/2;

		vertical = Vector3.Dot(targetPos.normalized, transform.forward);

		horizontal = Mathf.Clamp(horizontal, -1, 1);
		vertical = Mathf.Clamp(vertical, -1, 1);


		//Quaternion old = transform.rotation;
		//
		//transform.LookAt(m_Target);
		//float angle = transform.rotation.eulerAngles.y - old.eulerAngles.y;
		//transform.rotation = old;
		//
		//if(angle >= 180) {
		//	angle -= 360;
		//}
		//
		////90 instead of 180 to double the turn amount
		//angle /= 90;
		//
		//angle = Mathf.Clamp(angle, -1, 1);
		//
		//print(angle);
		//
		//horizontal = angle;
		//vertical = horizontal;

		//Vector3 perp = Vector3.Cross(transform.position + transform.forward, m_Target.position);
		//float dir = Vector3.Dot(perp, transform.up);
		//
		//float dist = Vector3.Distance(new Vector3(transform.position.x,0, transform.position.z), new Vector3(m_Target.position.x, 0, m_Target.position.z));
		//
		//
		////horizontal = -dir;
		//horizontal = Mathf.Clamp((dir/dist)*-1, -1, 1);
		//
		//print("angle: "+ dir + " dist: " + dist + " = " + dir/dist );

		base.moveCar();
	}

	//returns -1 when to the left, 1 to the right, and 0 for forward/backward
	public float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up) {
		Vector3 perp = Vector3.Cross(fwd, targetDir);
		float dir = Vector3.Dot(perp, up);

		return dir;
		//if (dir > 0.0f) {
		//	return 1.0f;
		//}
		//else if (dir < 0.0f) {
		//	return -1.0f;
		//}
		//else {
		//	return 0.0f;
		//}
	}

}
