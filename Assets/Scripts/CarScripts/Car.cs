﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody),typeof(UpsideDownFlip))]
public abstract class Car : MonoBehaviour {

	public float m_Power = 15;
	public float m_MaxSpeed = 2;
	public float m_TurnPower = 2;
	public float m_Friction = 0.5f;
	[HideInInspector]
	public Vector3 m_CurrSpeed;
	Rigidbody m_Rigidbody;

    public bool isOnGround { get { return m_IsOnGround; } }

    [HideInInspector]
    private bool m_IsOnGround = false;

	private Transform m_CarObject;

	[HideInInspector]
	public float vertical;
	[HideInInspector]
	public float horizontal;

	// Use this for initialization
	protected virtual void Start() {
		m_Rigidbody = GetComponent<Rigidbody>();
		//for(int i = 0; i < transform.childCount; i++) {
		//	if (transform.GetChild(i).CompareTag("Car")){
		//		m_CarObject = transform.GetChild(i);
		//		break;
		//	}
		//}
		m_CarObject = transform;
	}

    public virtual void moveCar() {

		//Check if the car is on the ground
		RaycastHit hit;
		//int layer_mask = LayerMask.GetMask("Ground");
		Debug.DrawRay(m_CarObject.position, -m_CarObject.up, Color.red);
		if (!Physics.Raycast(m_CarObject.position + Vector3.up, -m_CarObject.up, out hit, 2)) {
            m_IsOnGround = false;
			print("not on ground");
			return;
		}
        m_IsOnGround = true;

		//todo over hall this to be more arcadey

		//drive car based on information from vertical and horizontal

		m_CurrSpeed = m_Rigidbody.velocity;

		if (m_CurrSpeed.magnitude > m_MaxSpeed) {
			m_CurrSpeed = m_CurrSpeed.normalized * m_MaxSpeed;
		}

		if (vertical != 0) {
			if (vertical > 0) {
				m_Rigidbody.AddForce(transform.forward * m_Power);
				m_Rigidbody.drag = m_Friction;
			} else {
				//m_Rigidbody.AddForce((-transform.forward) * (m_Power / 2));
				m_Rigidbody.AddForce(transform.forward * -m_Power);
				m_Rigidbody.drag = m_Friction;
			}
		} else {
			m_Rigidbody.drag = m_Friction * 2;
		}

		//TODO: use Torque instead of directly rotating
		float rotationAmount = (m_CurrSpeed.magnitude / m_MaxSpeed);
		rotationAmount = Mathf.Clamp(rotationAmount, 0.1f, 1.0f);
		//m_Rigidbody.AddTorque(Vector3.up * horizontal * rotationAmount * m_TurnPower);
		transform.Rotate(Vector3.up * horizontal * rotationAmount * m_TurnPower);
	}

	public void LateUpdate() {
		moveCar();
	}
}
